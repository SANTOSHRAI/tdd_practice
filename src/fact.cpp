#include "fact.hpp"

int fact(int i)
{
	int ret_fact_value = 1;
	while(i-1)
	{
		ret_fact_value = ret_fact_value * i;
		i--;
	}
	return ret_fact_value;
}
